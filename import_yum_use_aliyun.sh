#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/16  12:32
#-------------------------------------------------
#   Change Activity:
#                    2018/7/16  12:32:
#-------------------------------------------------
#"""

set -e
#set -e

#-------------------------------------------------
LOGS(){
    echo [$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt
}
#-------------------------------------------------

yum_init(){
    yum clean all
    yum makecache -y
    yum repolist
}
#yum_init

LOGS "Install aliyun yum repo"
yum install wget -y
[[ -f /etc/yum.repos.d/Centos7-ali.repo ]] && mv /etc/yum.repos.d/Centos7-ali.repo{,.bak$(date +%Y%m%d%H%M%S)}
wget -O /etc/yum.repos.d/Centos7-ali.repo https://mirrors.aliyun.com/repo/Centos-7.repo
#yum_init

LOGS "Install repe yum repo"
[[ $(yum install epel-release -y) ]] || yum_init && yum install epel-release -y

yum_init