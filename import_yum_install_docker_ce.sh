#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/16  12:35
#-------------------------------------------------
#   Change Activity:
#                    2018/7/16  12:35:
#                    Get it: https://www.docker.com/community-edition
#-------------------------------------------------
#"""

#set -x
set -e

#-------------------------------------------------
LOGS(){
    echo [$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt
}
#-------------------------------------------------

LOGS "Install docker-ce-selinux-17.03.2.ce"
yum clean all
[[ $(command -v docker) ]] && LOGS $(command -v docker) && \
    [[ $(docker --version | cut -d ' ' -f 3) == '17.03.2-ce,' ]] || LOGS $(docker --version | cut -d ' ' -f 3) && \
    systemctl stop docker && LOGS "stop docker server" && \
    echo $(rpm -aq|grep docker) >> data.txt && \
    yum remove -y docker \
                docker-ce \
                docker-ce-selinux \
                container-selinux \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-selinux \
                docker-engine-selinux \
                docker-engine


LOGS "Clear old docker data"
[[ -d /var/lib/docker ]] && mv /var/lib/docker{,.bak_$(date +%Y%m%d%H%M%S)}

LOGS "Install docker-ce-selinux-17.03.2.ce"
[[ $(yum install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-selinux-17.03.2.ce-1.el7.centos.noarch.rpm -y) ]] || exit 0

LOGS "Install docker-ce-17.03.2.ce"
yum install --setopt=obsoletes=0 docker-ce-17.03.2.ce -y

LOGS "config images repository"
/bin/bash import_docker_registry_aliyun.sh

LOGS "Restart docker daemon"
systemctl daemon-reload
systemctl restart docker
systemctl enable docker
