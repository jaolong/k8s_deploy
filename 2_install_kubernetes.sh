#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/18  14:56
#-------------------------------------------------
#   Change Activity:
#                    2018/7/18  14:56:
#-------------------------------------------------
#"""

#set -x
set -e

#-------------------------------------------------
LOGS(){
    echo "[$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt"
}
#-------------------------------------------------

LOGS "Check yum source"
[[ -f /etc/yum.repos.d/kubernetes.repo ]] || [[ -f import_yum_kubernetes_use_aliyun.sh ]] ||
{
    echo "No kubernetes yum source is config" && exit 0
}

[[ -f /etc/yum.repos.d/kubernetes.repo ]] || [[ -f import_yum_kubernetes_use_aliyun.sh ]] && /bin/bash import_yum_kubernetes_use_aliyun.sh

LOGS "Install kubernetes"
#如果有一个版本号不匹配(包括包没有安装),都会进行卸载操作
[[ $(rpm -q kubectl-1.10.3-0.x86_64) == 'kubectl-1.10.3-0.x86_64' && \
    $(rpm -q kubelet-1.10.3-0.x86_64) == 'kubelet-1.10.3-0.x86_64' && \
    $(rpm -q kubernetes-cni-0.6.0-0.x86_64) == 'kubernetes-cni-0.6.0-0.x86_64' && \
    $(rpm -q kubeadm-1.10.3-0.x86_64s) == 'kubeadm-1.10.3-0.x86_64' ]] ||
{
    yum -y remove kubectl kubelet kubernetes kubeadm && \
    yum install -y kubelet-1.10.3 kubeadm-1.10.3 kubectl-1.10.3 socat-1.7.3.2-2.el7
}

LOGS "Chmod cgroupfs and dns networks"
sed -i "s/cgroup-driver=systemd/cgroup-driver=cgroupfs/g" /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sed -i "s/cluster-dns=10.96.0.10/cluster-dns=192.168.201.0/g" /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

LOGS "Start kubernetes"
systemctl enable kubelet && systemctl start kubelet

