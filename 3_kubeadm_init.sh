#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/18  17:40
#-------------------------------------------------
#   Change Activity:
#                    2018/7/18  17:40:
#-------------------------------------------------
#"""

#set -x
set -e

#-------------------------------------------------
LOGS(){
    echo "[$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt"
}
#-------------------------------------------------

# Reset firstly if ran kubeadm init before
kubeadm reset

LOGS "检查是否有安装cni"
[[ $(ip link | awk -F ':' '/flannel.*:/{print substr($2,1)}') ]] || \
[[ $(ip link | awk -F ':' '/cni.*:/{print substr($2,1)}') ]] &&
{
    ip link del $(ip link | awk -F ':' '/flannel.*:/{print substr($2,1)}')
    ip link del $(ip link | awk -F ':' '/cni.*:/{print substr($2,1)}')
}

# kubeadm init with flannel network
LOGS "kubeadm init with flannel network"
kubeadm init --kubernetes-version=v1.10.3 --pod-network-cidr=192.168.202.0/16

# regular user
user[0]="root:/root"
user[1]="k8s:/home/k8s"
user[2]="docker:/home/docker"

for user in ${user[@]};do
    #set $(echo ${user} | set 's/:/ /g')
    user_name=$(echo ${user%%:*})
    user_home=$(echo ${user##*:})
    LOGS "${user_name}:${user_home}"

    mkdir -p ${user_home}/.kube
    cp /etc/kubernetes/admin.conf ${user_home}/.kube/config
    chown -R $(id -u ${user_name}):$(id -g ${user_name}) ${user_home}/.kube/

    cp -p ${user_home}/.bash_profile ${user_home}/.bash_profile.bak_$(date +"%Y%m%d_%H%M%S")
    echo "export KUBECONFIG=${user_home}/.kube/config" >> ${user_home}/.bash_profile
    source ${user_home}/.bash_profile
done
