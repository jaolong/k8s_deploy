#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/16  12:34
#-------------------------------------------------
#   Change Activity:
#                    2018/7/16  12:34:
#-------------------------------------------------
#"""

#set -x
set -e

#-------------------------------------------------
LOGS(){
    echo [$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt
}
#-------------------------------------------------

#yum remove docker docker-common container-selinux -y
yum install yum-utils device-mapper-persistent-data lvm2 -y
[[ $(yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo) ]] || \
yum clean all && yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
