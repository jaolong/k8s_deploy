#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/18  16:01
#-------------------------------------------------
#   Change Activity:
#                    2018/7/18  16:01:
#-------------------------------------------------
#"""

#set -x
set -e

#-------------------------------------------------
LOGS(){
    echo "[$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt"
}
#-------------------------------------------------

create_repo_file(){
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
}

[[ -f /etc/yum.repos.d/kubernetes.repo ]] || create_repo_file

yum clean all