#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/19  10:46
#-------------------------------------------------
#   Change Activity:
#                    2018/7/19  10:46:
#-------------------------------------------------
#"""

set -x
#set -e

#-------------------------------------------------
LOGS(){
    echo "[$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt"
}
#-------------------------------------------------

LOGS "检查是否有安装"
[[ $(ip link | awk -F ':' '/flannel.*:/{print substr($2,1)}') ]] || \
[[ $(ip link | awk -F ':' '/cni.*:/{print substr($2,1)}') ]] && exit 0

LOGS "下载flannel.yml"
[[ -f kube-flannel.yml ]] || \
wget https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml

LOGS "修改flannel信息"
sed -i 's#"Network": "10.244.0.0/16",#"Network": "192.168.202.0/16",#g' kube-flannel.yml
sed -i 's#image: quay.io/coreos/flannel:v0.10.0-amd64#image: quay.io/coreos/flannel:v0.10.0#g' kube-flannel.yml

LOGS "安装flannel"
kubectl apply -f kube-flannel.yml
sleep 10s

ifconfig

LOGS "Check component status"
kubectl get cs

LOGS "Check pods status incase any pods are not in running status"
kubectl get pods --all-namespaces



