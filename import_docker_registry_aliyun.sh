#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/18  16:54
#-------------------------------------------------
#   Change Activity:
#                    2018/7/18  16:54:
#-------------------------------------------------
#"""

set -x
#set -e

#-------------------------------------------------
LOGS(){
    echo "[$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt"
}
#-------------------------------------------------

mkdir -p /etc/docker

[[ -f /etc/docker/daemon.json ]] && cp /etc/docker/daemon.json{,.bak_$(date +%Y%m%d_%H%M%S)}

cat <<EOF > /etc/docker/daemon.json
{
    "registry-mirrors": ["http://hub-mirror.c.163.com", "https://registry.docker-cn.com"],
    "bip": "192.168.200.1/16",
    "max-concurrent-downloads": 20
}
EOF
