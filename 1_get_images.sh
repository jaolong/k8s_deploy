#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/17  12:24
#-------------------------------------------------
#   Change Activity:
#                    2018/7/17  12:24:
#-------------------------------------------------
#"""

#set -x
set -e

#-------------------------------------------------
LOGS(){
    echo "[$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt"
}
#-------------------------------------------------

## Check version in https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/
## Search "Running kubeadm without an internet connection"
## For running kubeadm without an internet connection you have to pre-pull the required master images for the version of choice:

#---------------------------------------从docke images生成镜像数组---------------------------------------#
creat_data(){
    #生成已有镜像数组
    #  从成功运行的镜像列表中生成数组
    docker images|awk -F ' ' '{print "data["NR"]="$1":"$2}'
    #
    data[5]=k8s.gcr.io/kube-proxy-amd64:v1.10.3
    data[6]=k8s.gcr.io/kube-scheduler-amd64:v1.10.3
    data[7]=k8s.gcr.io/kube-apiserver-amd64:v1.10.3
    data[8]=k8s.gcr.io/kube-controller-manager-amd64:v1.10.3
    data[9]=gcr.io/google_containers/heapster-amd64:v1.5.3
    data[10]=k8s.gcr.io/etcd-amd64:3.1.12
    data[11]=k8s.gcr.io/kubernetes-dashboard-amd64:v1.8.3
    data[12]=quay.io/coreos/flannel:v0.10.0
    data[13]=k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64:1.14.8
    data[14]=k8s.gcr.io/k8s-dns-sidecar-amd64:1.14.8
    data[15]=k8s.gcr.io/k8s-dns-kube-dns-amd64:1.14.8
    data[16]=k8s.gcr.io/pause-amd64:3.1
    data[17]=gcr.io/google_containers/heapster-influxdb-amd64:v1.3.3
    data[18]=gcr.io/google_containers/heapster-grafana-amd64:v4.4.3
    data[19]=k8s.gcr.io/kubernetes-dashboard-amd64:v1.8.3
    #
    ##生成0开始的标准数组
    COUNT=0
    for image in ${data[@]};do
        #new_image=$(echo ${image}|sed 's/\(.*\[\).*\(\].*\)/\1'${COUNT}'\2/g')
         DOCKER_URL=$(echo ${image%/*})
         IMAGE_NAME=$(echo ${image##*/})
        echo data[${COUNT}]=\$\{IMAGE_URL\}:${DOCKER_URL}:${IMAGE_NAME}
        COUNT=$(expr ${COUNT} + 1)
    done
}

#------------------------------------------获取镜像------------------------------------------#
get_images(){
    #镜像URL
    # docker hub
    #IMAGE_URL=snowdk
    # 163 hub
    #IMAGE_URL=hub.c.163.com/showyi
    # ali yun
    IMAGE_URL=registry.cn-shanghai.aliyuncs.com/kubeimage

    #镜像信息数据组
    #自有镜像URL:官方镜像URL:镜像名称:镜像版本
    data[0]=${IMAGE_URL}:k8s.gcr.io:kube-proxy-amd64:v1.10.3
    data[1]=${IMAGE_URL}:k8s.gcr.io:kube-scheduler-amd64:v1.10.3
    data[2]=${IMAGE_URL}:k8s.gcr.io:kube-apiserver-amd64:v1.10.3
    data[3]=${IMAGE_URL}:k8s.gcr.io:kube-controller-manager-amd64:v1.10.3
    data[4]=${IMAGE_URL}:gcr.io/google_containers:heapster-amd64:v1.5.3
    data[5]=${IMAGE_URL}:k8s.gcr.io:etcd-amd64:3.1.12
    data[6]=${IMAGE_URL}:k8s.gcr.io:kubernetes-dashboard-amd64:v1.8.3
    data[7]=${IMAGE_URL}:quay.io/coreos:flannel:v0.10.0
    data[8]=${IMAGE_URL}:k8s.gcr.io:k8s-dns-dnsmasq-nanny-amd64:1.14.8
    data[9]=${IMAGE_URL}:k8s.gcr.io:k8s-dns-sidecar-amd64:1.14.8
    data[10]=${IMAGE_URL}:k8s.gcr.io:k8s-dns-kube-dns-amd64:1.14.8
    data[11]=${IMAGE_URL}:k8s.gcr.io:pause-amd64:3.1
    data[12]=${IMAGE_URL}:gcr.io/google_containers:heapster-influxdb-amd64:v1.3.3
    data[13]=${IMAGE_URL}:gcr.io/google_containers:heapster-grafana-amd64:v4.4.3
    data[14]=${IMAGE_URL}:k8s.gcr.io:kubernetes-dashboard-amd64:v1.8.3

    #pull镜像并修改tag
    for imageName in ${data[@]};do
        set $(echo ${imageName} | sed 's/:/ /g')
        docker pull $1/$3:$4
        docker tag $1/$3:$4 $2/$3:$4
        docker rmi $1/$3:$4
    done

    #查看镜像列表
    docker images
}
#------------------------------------------从163 hub获取镜像------------------------------------------#
# for i in $(docker images |awk -F ' ' 'NR>1{print $1":"$2}');do docker tag $i hub.c.163.com/showyi/$(echo ${i##*/});docker push hub.c.163.com/showyi/$(echo ${i##*/});docker rmi hub.c.163.com/showyi/$(echo ${i##*/});done

#------------------------------------------从ali hub获取镜像------------------------------------------#
# for i in $(docker images |awk -F ' ' 'NR>1{print $1":"$2}');do docker tag $i registry.cn-shanghai.aliyuncs.com/kubeimage/$(echo ${i##*/});docker push registry.cn-shanghai.aliyuncs.com/kubeimage/$(echo ${i##*/});docker rmi registry.cn-shanghai.aliyuncs.com/kubeimage/$(echo ${i##*/});done

# 创建镜像数组
#creat_data

#获取镜像列表
get_images