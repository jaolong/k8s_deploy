#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/16  11:14
#-------------------------------------------------
#   Change Activity:
#                    2018/7/16  11:14:
#-------------------------------------------------
#"""

#set -x
set -e

#-------------------------------------------------
LOGS(){
    echo [$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt
}
#-------------------------------------------------

LOGS "ADD docker user"
[[ $(id docker) ]] || useradd -m docker -g docker
LOGS $(id docker)

LOGS "ADD k8s user"
[[ $(id k8s) ]] || useradd -m k8s -g docker -G wheel

LOGS "ADD k8s user NOPASSWD"
cp /etc/sudoers{,.bak$(date +%Y%m%d%H%M%S)}
[[ $(sed -n '/^%wheel.*ALL=(ALL).*ALL/p' /etc/sudoers) ]] && sed -i 's/^%wheel.*ALL=(ALL).*ALL/#&/' /etc/sudoers

[[ $(sed -n '/^#.*%wheel.*ALL=(ALL).*NOPASSWD:.*ALL/p' /etc/sudoers) ]] && sed -i 's/^#\(.*%wheel.*ALL=(ALL).*NOPASSWD:.*ALL\)/\1/g' /etc/sudoers

