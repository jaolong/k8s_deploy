#!/usr/bin/bash
#
#"""
#-------------------------------------------------
#   File Name:       ${NAME}
#   Description:
#   Project Name:    rsync_cfg
#   Ide Name:        PyCharm
#   Author:          pulw
#   date:            2018/7/13  16:28
#-------------------------------------------------
#   Change Activity:
#                    2018/7/13  16:28:
#-------------------------------------------------
#"""

set -e

#-------------------------------------------------
LOGS(){
    echo [$(date +%Y%m%d_%H:%M:%S) $$] $* | tee -a logs.txt
}
#-------------------------------------------------

LOGS "###############################################"
LOGS "Please ensure run this script with root user"
[ $(id -u) == 0 ] || exit 0

LOGS "###############################################"
LOGS "Please ensure your OS is CentOS7 64 bits"
[[ $(cat /etc/redhat-release) =~ ^"CentOS" ]] || exit 0
LOGS $(cat /etc/redhat-release)

LOGS "###############################################"
LOGS "Please ensure your machine has full network connection and internet access"
ping www.baidu.com -c 1
[[ $? == 0 ]] || exit 0

LOGS "###############################################"
LOGS "Please check ip addr as below:"
HOST_IFACES=$(route | grep default |awk -F ' ' '{print $NF}' | head -n 1)
HOST_IP=$(ifconfig ${HOST_IFACES} | grep -w inet | awk  -F ' ' '{print $2}')
LOGS "IP: ${HOST_IP}"

LOGS "###############################################"
LOGS "Please check Mac addr and product_uuid as below:"
ip link
sudo cat /sys/class/dmi/id/product_uuid

# Check kernel-name
LOGS "###############################################"
LOGS "Please check kernel-name as below:"
SYSTEM_TYPE=$(uname -s)
[[ $(uname -s) == "Linux" ]] || exit 0
LOGS $(uname -s)

# Check processor type
LOGS "###############################################"
LOGS "Please check processor type as below:"
SYSTEM_CORE_VERSION=$(uname -m)
[[ $(uname -m) == "x86_64" ]] || exit 0
LOGS $(uname -m)

# Check hosts file
LOGS "###############################################"
LOGS "Please check hosts file as below:"
SYSTEM_HOSTNAME=$(uname -n)
[[ $(sed -n "/${HOST_IP}\(.*\)${SYSTEM_HOSTNAME}/p" /etc/hosts) ]] || \
echo "${HOST_IP} ${SYSTEM_HOSTNAME}" >> /etc/hosts || exit 0

LOGS $(sed -n "/${HOST_IP}\(.*\)${SYSTEM_HOSTNAME}/p" /etc/hosts)

# Stop firewalld
LOGS "###############################################"
LOGS "Stop firewalld"
systemctl stop firewalld
systemctl disable firewalld

# Disable SELinux
LOGS "###############################################"
LOGS "Disable SELinux"
#setenforce 0
[[ $(sed -n "/^SELINUX=enforcing/p" /etc/selinux/config) ]] && cp -p /etc/selinux/config /etc/selinux/config.bak$(date '+%Y%m%d%H%M%S')
sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config

LOGS  $(sed -n "/^SELINUX=disabled/p" /etc/selinux/config)

# Turn off Swap
LOGS "###############################################"
LOGS "Turn off Swap"
swapoff -a
cp -p /etc/fstab /etc/fstab.bak$(date '+%Y%m%d%H%M%S')
sed -i "s/\/dev\/mapper\/rhel-swap/\#\/dev\/mapper\/rhel-swap/g" /etc/fstab
sed -i "s/\/dev\/mapper\/centos-swap/\#\/dev\/mapper\/centos-swap/g" /etc/fstab
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
mount -a
free -m
cat /proc/swaps

LOGS "###############################################"
LOGS "Turn off dnsmasq"
[[ $(rpm -q dnsmqsq) ]] && \
systemctl stop dnsmasq && \
systemctl disable dnsmasq

# Setup iptables (routing)
LOGS "###############################################"
LOGS "Setup iptables (routing)"
[[ -f /etc/sysctl.d/k8s.conf ]] && cp /etc/sysctl.d/k8s.conf{,.bak$(date '+%Y%m%d%H%M%S')}
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-arptables = 1
net.ipv4.ip_forward = 1
vm.swappiness = 0
vm.overcommit-memory = 1
vm.panic_on_oom = 0
EOF

sysctl --system

[[ $(ls /sys/devices/virtual/net/docker0/brif/*) ]] && for intf in /sys/devices/virtual/net/docker0/brif/*; do echo 1 > $intf/hairpin_mode; done

LOGS "###############################################"
LOGS "load system mode"
modprobe br_netfilter
modprobe ip_vs

LOGS "###############################################"
LOGS "set system date"
timedatectl set-timezone Asia/Shanghai
timedatectl set-local-rtc 0
systemctl restart rsyslog
systemctl restart crond

# Use Aliyun Yum source
LOGS "###############################################"
LOGS "Use Aliyun Yum source"
#./use_aliyun_yum_source.sh
/bin/bash import_yum_use_aliyun.sh

# Install docker-ce repo
LOGS "###############################################"
LOGS "Install docker-ce repo"
/bin/bash import_yum_docker_use_aliyun.sh

# Install docker-ce-selinux 17
LOGS "###############################################"
LOGS "Install docker-ce-selinux 17"
/bin/bash import_yum_install_docker_ce.sh

LOGS "###############################################"
LOGS "handler docker user"
/bin/bash import_user.sh

LOGS "###############################################"
LOGS "Install depend packages"
yum install -y conntrack ipvsadm ipset jq sysstat curl iptables


LOGS "###############################################"
LOGS "Check system environment"
[[ -f check-config.sh ]] || \
curl https://raw.githubusercontent.com/docker/docker/master/contrib/check-config.sh > check-config.sh && \
chmod +x check-config.sh

/bin/bash check-config.sh


